package br.fatec.app01;

import android.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText editValueOne;
    private EditText editValueTwo;
    private Button btnCalculate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editValueOne = (EditText) findViewById(R.id.edtValor1);
        editValueTwo = (EditText) findViewById(R.id.edtValor2);
        btnCalculate = (Button) findViewById(R.id.btnCalcular);

        btnCalculate.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {

        if(v.getId()==R.id.btnCalcular){
            double v1 = Double.parseDouble(editValueOne.getText().toString());
            double v2 = Double.parseDouble(editValueTwo.getText().toString());
            double res = (v1 + v2);
            AlertDialog.Builder dlg = new AlertDialog.Builder(this);
            dlg.setPositiveButton("ok",null);
           // dlg.setMessage(re);
            dlg.show();

        }
    }
}

