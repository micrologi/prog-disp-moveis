package br.fatec.app01;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class TelaPrincipal extends AppCompatActivity implements View.OnClickListener{

    //Declarar os controles da UI que serão programados
    private EditText edtValor1;
    private EditText edtValor2;
    private Button btnCalcular;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_principal);

        //Referenciar os controles da UI
        edtValor1 = (EditText)findViewById(R.id.edtValor1);
        edtValor2 = (EditText)findViewById(R.id.edtValor2);
        btnCalcular = (Button)findViewById(R.id.btnCalcular);

        //Indicar que o evento click será tratado para o btnCalcular
        btnCalcular.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {

        //se o btnCalcular foi clicado
        if (view.getId() == R.id.btnCalcular){

            //retornar os valores dos EditText
            double v1 = Double.parseDouble(
              edtValor1.getText().toString());

            double v2 = Double.parseDouble(
              edtValor2.getText().toString());

            double res = (v1+v2);

            AlertDialog.Builder dlg = new AlertDialog.Builder(this);
            dlg.setMessage(String.format("%.2f", res));
            dlg.setPositiveButton("ok",null);
            dlg.show();



        }


    }
}
