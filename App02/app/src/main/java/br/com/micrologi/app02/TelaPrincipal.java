package br.com.micrologi.app02;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Switch;

/*Adicionei "implements View.OnClickListener" e ALT+ENTER - Implement Methods */
public class TelaPrincipal extends AppCompatActivity implements View.OnClickListener {

    private Button btnAdicionar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_principal);

        /*R => Classe que contem todos os elementos/componentes do XML da tela*/
        btnAdicionar = (Button)findViewById(R.id.btnAdicionar);
        btnAdicionar.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.btnAdicionar) {

            //Definir a estrategia de navegacao
            Intent it = new Intent(
              getApplicationContext(),  //Nome da Tela de origem/que estamos, essa função retorna isso
              TelaFinalizar.class       //Tela de destino para onde vai
            );

            /*Sabor*/
            Spinner spnSabor = (Spinner)findViewById(R.id.spnSabor);
            int posicao = spnSabor.getSelectedItemPosition();
            String valor = spnSabor.getSelectedItem().toString();

            it.putExtra("sabor",valor);

            /*Tamanho e Preço*/
            double preco = 0;

            RadioGroup rdgTamanho = (RadioGroup)findViewById(R.id.rdgTamanho);
            switch (rdgTamanho.getCheckedRadioButtonId()) {
                case R.id.rdbPequeno:
                    it.putExtra("tamanho","Pequeno");
                    preco = 30.00;
                    break;
                case R.id.rdbMedio:
                    it.putExtra("tamanho","Médio");
                    preco = 45.00;
                    break;
                case R.id.rdbGrande:
                    it.putExtra("tamanho","Grande");
                    preco = 85.00;
                    break;
            }

            /*Adicional borda*/
            String adicionais = "";
            Double preco_adicional = 0.00;

            CheckBox ckbBorda = (CheckBox)findViewById(R.id.ckbBorda);
            if (ckbBorda.isChecked()) {
                adicionais += "Borda recheada;";
                preco_adicional += 0.05;
            }

            CheckBox ckbQueijo = (CheckBox)findViewById(R.id.ckbQueijo);
            if (ckbQueijo.isChecked()) {
                adicionais += "Queijo;";
                preco_adicional += 0.03;
            }

            CheckBox ckbBacon = (CheckBox)findViewById(R.id.ckbBacon);
            if (ckbBacon.isChecked()) {
                adicionais += "Bacon;";
                preco_adicional += 0.05;
            }

            CheckBox ckbDobro = (CheckBox)findViewById(R.id.ckbDobro);
            if (ckbDobro.isChecked()) {
                adicionais += "Recheio em Dobro;";
                preco_adicional += 0.12;
            }

            CheckBox ckbSemCebola = (CheckBox)findViewById(R.id.ckbSemCebola);
            if (ckbSemCebola.isChecked()) {
                adicionais += "Sem Cebola;";
            }

            Switch swt = (Switch)findViewById(R.id.swtRetirar);
            if (swt.isChecked()) {
                it.putExtra("retirar", "Sim");
            } else {
                it.putExtra("retirar", "Nao");
            }

            Double preco_final;
            preco_final = preco + (preco * preco_adicional);
            it.putExtra("adicionais",adicionais);
            it.putExtra("preco_final",preco_final);

            //Abrir a tela Finalizar
            startActivity(it);

        }

    }
}
