package br.com.micrologi.app02;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class TelaFinalizar extends AppCompatActivity implements View.OnClickListener {

    private Button btnFinalizar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_finalizar);

        btnFinalizar = (Button)findViewById(R.id.btnFinalizar);
        btnFinalizar.setOnClickListener(this);

        Intent it = getIntent();
        if (it != null) {
            EditText edtDescricao = (EditText)findViewById(R.id.edtDescricao);
            EditText edtPreco = (EditText)findViewById(R.id.edtPreco);

            String descricao =
                    "Sabor: " + it.getStringExtra("sabor") + "\n" +
                            "Tamanho: " + it.getStringExtra("tamanho") + "\n" +
                            "Adicionais: " + it.getStringExtra("adicionais") + "\n" +
                            "Retirar no balcão: " + it.getStringExtra("retirar");
            edtDescricao.setText(descricao);


            Double preco_final = it.getDoubleExtra("preco_final",0);
            edtPreco.setText(
                    String.format("R$ %.2f",preco_final)
            );

        }
    }

    @Override
    public void onClick(View v) {

    }
}
